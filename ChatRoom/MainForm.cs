﻿using System;
using System.Drawing;
using System.Windows.Forms;
//
using ChatRoom.Properties;
using ECP.PersianMessageBox;
//
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ChatRoom
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        #region MainForm
        //----------

        #region FormDragwithMouse

        private void frmDragForm_Paint(object sender, PaintEventArgs e)
        {
            //Draws a border to make the Form stand out
            //Just done for appearance, not necessary

            Pen p = new Pen(Color.Gray, 3);
            e.Graphics.DrawRectangle(p, 0, 0, this.Width - 1, this.Height - 1);
            p.Dispose();
        }

        Point lastClick; //Holds where the Form was clicked

        private void frmDragForm_MouseDown(object sender, MouseEventArgs e)
        {
            lastClick = new Point(e.X, e.Y); //We'll need this for when the Form starts to move
        }

        private void frmDragForm_MouseMove(object sender, MouseEventArgs e)
        {
            //Point newLocation = new Point(e.X - lastE.X, e.Y - lastE.Y);
            if (e.Button == MouseButtons.Left) //Only when mouse is clicked
            {
                //Move the Form the same difference the mouse cursor moved;
                this.Left += e.X - lastClick.X;
                this.Top += e.Y - lastClick.Y;
            }
        }

        //If the user clicks on the Objects(on the form) we want the same kind of behavior
        //so we just call the Forms corresponding methods
        private void frmDragObjects_MouseDown(object sender, MouseEventArgs e)
        {
            frmDragForm_MouseDown(sender, e);
        }

        private void frmDragObjects_MouseMove(object sender, MouseEventArgs e)
        {
            frmDragForm_MouseMove(sender, e);
        }

        #endregion FormDragwithMouse

        ////----------

        #region toolStripMenu

        private void btnExit_Click(object sender, EventArgs e)
        {
            if (PersianMessageBox.Show("آیا می‌خواهید خارج شوید؟   \n",
                                       "خروج",
                                       PersianMessageBox.Buttons.YesNo,
                                       PersianMessageBox.Icon.Question,
                                       PersianMessageBox.DefaultButton.Button2) == DialogResult.Yes)
            {
                System.Globalization.CultureInfo languageEn = new System.Globalization.CultureInfo("en-US");
                InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(languageEn);

                Application.Exit();
            }
        }

        private void btnMinimized_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            Form Info = new InfoForm();
            Info.Show();
            MPG_WinShutDowm.ShowShutForm sf = new MPG_WinShutDowm.ShowShutForm(new InfoForm());
            Info.Close();
        }

        #endregion toolStripMenu

        ////----------

        #region toolStripLogo

        private void toolStripLogo_MouseHover(object sender, EventArgs e)
        {
            this.toolStripLogo.Cursor = System.Windows.Forms.Cursors.SizeAll;
        }

        #endregion toolStripLogo

        //----------
        #endregion MainForm

        #region TryCatch
        //----------

        private bool ShowFriendlyMessage = true;
        private string MessageConError = "خطا در برقراری ارتباط با پایگاه‌داده.   ";
        private string MessageExpErrorSorry = "\n\n-متاسفیم.   \n";

        private string MesExpErrTabSv = string.Empty;
        private string MesExpErrTabCl = string.Empty;

        private static string MesErrTS = "\n-خطای ۱";
        private static string MesErrTC = "\n-خطای ۲";

        //----------
        /*Algoritm ShowMessageError
        string ShowMessageError = "a-bc-d-e";
        
            'a' is for specification Tabs Program
                1 = Server
                2 = Client
                3 = noTab
        
            'bc' is for Agency-Number [errors] each Function
        
            'd' is for specification Querys:
                0 = noQuery
                1 = SELECT
                2 = INSERT
                3 = UPDATE
                4 = DELETE
                
            'e' is for Agency-Number errors Sections-Function
                first Section is '0'
        
        */
        //----------

        private bool CatchExceptionMError(string MessageError)
        {
            bool isError = false;

            if (MessageError != "")
            {
                isError = true;

                MessageError += MessageExpErrorSorry;

                PersianMessageBox.Show(MessageError,
                                       "خطا",
                                       PersianMessageBox.Buttons.OK,
                                       PersianMessageBox.Icon.Error,
                                       PersianMessageBox.DefaultButton.Button1);

                MessageError = "";
            }

            return isError;
        }

        //----------
        #endregion TryCatch

        #region ServerCodes
        //----------

        #region ServerData
        //----------

        Socket listnerSv;
        Socket handlerSv;
        IPHostEntry ipHostInfoSv;
        IPAddress ipAddSv;
        IPEndPoint localEndPointSv;

        const int maxClient = 10;

        Thread threadSv1;
        Thread threadSv2;

        string dataSv;

        byte[] msgSv;

        private delegate void setDisplaySv(string Text);//for lstMessageServer
        private delegate void EnableTrueSv(bool b);//for txtMsgServer & btnSendServer & lstMsg

        //----------
        #endregion ServerData

        private void btnCancleServer_Click(object sender, EventArgs e)
        {
            #region ServerClosing

            try
            {
                handlerSv.Shutdown(SocketShutdown.Both);
                listnerSv.Shutdown(SocketShutdown.Both);
                threadSv1.Abort();
                threadSv2.Abort();
            }
            catch
            {
            }

            #endregion ServerClosing

            EnableAfterConnectServer(false);

            picBoxServer.BackgroundImage = Resources.Pointers;
            lblCommentServer.Text = "می‌توانید گپی دوستانه را آغاز کنید!";
        }

        private void ConnectServer()
        {
            try
            {
                btnCancleServer.Visible = true;

                //lblCommentTextServer("Waiting for Connect a Client...");
                lblCommentTextServer("لطفا منتظر اتصال به Client باشید ...");
                ipHostInfoSv = Dns.Resolve(txtIPServer.Text);
                ipAddSv = ipHostInfoSv.AddressList[0];
                localEndPointSv = new IPEndPoint(ipAddSv, 1369);
                listnerSv.Bind(localEndPointSv);
                listnerSv.Listen(maxClient);

                threadSv1 = new Thread(new ThreadStart(AcceptStartServer));
                threadSv1.Start();

            }
            catch (Exception ex)
            {
                EnableAfterConnectServer(false);
                MessageBox.Show(ex.Message);
            }
        }

        private void AcceptStartServer()
        {
            try
            {
                handlerSv = listnerSv.Accept();
                threadSv2 = new Thread(new ThreadStart(DataReceiveServer));
                threadSv2.Start();

                EnableAfterConnectServer(true);
                System.Media.SystemSounds.Exclamation.Play();

                // String ClientIP = ((IPEndPoint)handler.RemoteEndPoint).Address.ToString();
            }
            catch (Exception ex)
            {
                EnableAfterConnectServer(false);
                MessageBox.Show(ex.Message);
            }
        }

        private void DataReceiveServer()
        {
            try
            {
                byte[] bytes = new byte[1000];
                int byteRec;

                while (true)
                {
                    while (true)
                    {
                        byteRec = handlerSv.Receive(bytes);
                        if (byteRec > 0)
                        {
                            dataSv = System.Text.Encoding.UTF8.GetString(bytes, 0, byteRec);
                            break;
                        }
                    }

                    FillLstMsgServer(dataSv);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lblCommentTextServer(string lblText)
        {
            btnConnectServer.Text = "منتظر";
            if (lblCommentServer.InvokeRequired == true)
            {
                setDisplaySv d = new setDisplaySv(lblCommentTextServer);
                this.Invoke(d, new object[] { lblText });
            }
            else
            {
                lblCommentServer.Text = lblText;
            }
        }

        private void EnableAfterConnectServer(bool b)
        {
            if (txtMsgServer.InvokeRequired == true)
            {
                EnableTrueSv et = new EnableTrueSv(EnableAfterConnectServer);
                this.Invoke(et, new object[] { b });
            }
            else
            {
                txtMsgServer.Enabled = b;
            }
            //----------
            if (btnSendMsgServer.InvokeRequired == true)
            {
                EnableTrueSv et = new EnableTrueSv(EnableAfterConnectServer);
                this.Invoke(et, new object[] { b });
            }
            else
            {
                btnSendMsgServer.Enabled = b;
            }
            //----------
            if (lstMsg.InvokeRequired == true)
            {
                EnableTrueSv et = new EnableTrueSv(EnableAfterConnectServer);
                this.Invoke(et, new object[] { b });
            }
            else
            {
                lstMsg.Enabled = b;
            }
            if (b)
            {
                btnConnectServer.Text = "متصل";
                picBoxServer.BackgroundImage = Resources.Conected;
                lblCommentServer.Text = "اتصال موفقیت‌آمیز بود!";
            }
            else
            {
                btnCancleServer.Visible = false;

                btnConnectServer.Text = "شروع";
                picBoxServer.BackgroundImage = Resources.notConected;
                lblCommentServer.Text = "اتصال ناموفق بود!";
            }
        }

        private void FillLstMsgServer(string str)
        {
            try
            {
                if (lstMsg.InvokeRequired == true)
                {
                    setDisplaySv d = new setDisplaySv(FillLstMsgServer);
                    this.Invoke(d, new object[] { str });
                }
                else
                {
                    lstMsg.Items.Add(str);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnConnectServer_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            btnConnectServer.Enabled = false;

            ConnectServer();

            btnConnectServer.Enabled = true;
            Cursor = Cursors.Default;
        }

        private void btnSendMsgServer_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMsgServer.Text != "")
                {
                    msgSv = System.Text.Encoding.UTF8.GetBytes(Dns.GetHostName() + ": " + txtMsgServer.Text);
                    handlerSv.Send(msgSv);
                    //
                    lstMsg.Items.Add("من: " + txtMsgServer.Text);
                    txtMsgServer.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtMsgServer_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnSendMsgServer_Click(this, EventArgs.Empty);
        }

        //----------
        #endregion ServerCodes

        #region ClientCodes
        //----------

        #region ClientData
        //----------

        Socket client;
        IPHostEntry ipHostInfoCl;
        IPAddress ipAddCl;
        IPEndPoint remoteEndPointCl;

        Thread threadCl;

        string dataCl;

        byte[] msgCl;

        private delegate void setDisplayCl(string Text);    //for lstMessage
        private delegate void EnableTrueCl(bool b);   //for txtMsgClient & btnSendClient & lstMsg

        //----------
        #endregion ClientData

        private void btnCancleClient_Click(object sender, EventArgs e)
        {
            #region ClientClosing

            try
            {
                client.Shutdown(SocketShutdown.Both);
                threadCl.Abort();
            }
            catch
            {
            }

            #endregion ClientClosing

            EnableAfterConnectClient(false);

            picBoxClient.BackgroundImage = Resources.Pointers;
            lblCommentClient.Text = "می‌توانید به گپی دوستانه بپیوندید!";
        }

        private void ConnectClient()
        {
            try
            {
                btnCancleClient.Visible = true;

                lblCommentTextClient("لطفا منتظر اتصال به Server باشید ...");
                ipHostInfoCl = Dns.Resolve(txtServerIPClient.Text);
                ipAddCl = ipHostInfoCl.AddressList[0];
                remoteEndPointCl = new IPEndPoint(ipAddCl, 1369);
                client.Connect(remoteEndPointCl);

                threadCl = new Thread(new ThreadStart(DataReceiveClient));
                threadCl.Start();

                EnableAfterConnectClient(true);
                System.Media.SystemSounds.Exclamation.Play();
            }
            catch (Exception ex)
            {
                EnableAfterConnectClient(false);
                MessageBox.Show(ex.Message);
            }
        }

        private void DataReceiveClient()
        {
            try
            {
                byte[] bytes = new byte[1000];
                int byteRec;

                while (true)
                {
                    while (true)
                    {
                        byteRec = client.Receive(bytes);
                        if (byteRec > 0)
                        {
                            dataCl = System.Text.Encoding.UTF8.GetString(bytes, 0, byteRec);
                            break;
                        }
                    }

                    FillLstMsgClient(dataCl);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FillLstMsgClient(string str)
        {
            try
            {
                if (lstMsg.InvokeRequired == true)
                {
                    setDisplayCl d = new setDisplayCl(FillLstMsgClient);
                    this.Invoke(d, new object[] { str });
                }
                else
                {
                    lstMsg.Items.Add(str);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void EnableAfterConnectClient(bool b)
        {
            if (txtMsgClient.InvokeRequired == true)
            {
                EnableTrueCl et = new EnableTrueCl(EnableAfterConnectClient);
                this.Invoke(et, new object[] { b });
            }
            else
            {
                txtMsgClient.Enabled = b;
            }
            //----------
            if (btnSendMsgClient.InvokeRequired == true)
            {
                EnableTrueCl et = new EnableTrueCl(EnableAfterConnectClient);
                this.Invoke(et, new object[] { b });
            }
            else
            {
                btnSendMsgClient.Enabled = b;
            }
            //----------
            if (lstMsg.InvokeRequired == true)
            {
                EnableTrueCl et = new EnableTrueCl(EnableAfterConnectClient);
                this.Invoke(et, new object[] { b });
            }
            else
            {
                lstMsg.Enabled = b;
            }
            if (b)
            {
                btnConnectClient.Text = "متصل";
                picBoxClient.BackgroundImage = Resources.Conected;
                lblCommentClient.Text = "اتصال موفقیت‌آمیز بود!";
            }
            else
            {
                btnCancleClient.Visible = false;

                btnConnectClient.Text = "جستجو ...";
                picBoxClient.BackgroundImage = Resources.notConected;
                lblCommentClient.Text = "اتصال ناموفق بود!";
            }
        }

        private void lblCommentTextClient(string btnText)
        {
            btnConnectClient.Text = "انتظار";
            if (lblCommentClient.InvokeRequired == true)
            {
                setDisplayCl d = new setDisplayCl(lblCommentTextClient);
                this.Invoke(d, new object[] { btnText });
            }
            else
            {
                lblCommentClient.Text = btnText;
            }
        }

        private void btnConnectClient_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            btnConnectClient.Enabled = false;

            ConnectClient();

            btnConnectClient.Enabled = true;
            Cursor = Cursors.Default;
        }

        private void btnSendMsgClient_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMsgClient.Text != "")
                {
                    msgCl = System.Text.Encoding.UTF8.GetBytes(Dns.GetHostName() + ": " + txtMsgClient.Text);
                    client.Send(msgCl);
                    //
                    lstMsg.Items.Add("من: " + txtMsgClient.Text);
                    txtMsgClient.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtMsgClient_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnSendMsgClient_Click(this, EventArgs.Empty);
        }

        //----------
        #endregion ClientCodes

        //------------
        ////----------////---------------------------------------------------------------------// Begin MainForm_Load
        //------------

        private void toolStripLogo_Enter(object sender, EventArgs e)
        {
            Icon = Resources.MainFrm;

            #region InputLanguage
            //----------

            System.Globalization.CultureInfo languagePe = new System.Globalization.CultureInfo("fa-IR");
            InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(languagePe);

            //----------
            #endregion InputLanguage

        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            #region ServerClosing

            e.Cancel = true;
            try
            {
                handlerSv.Shutdown(SocketShutdown.Both);
                listnerSv.Shutdown(SocketShutdown.Both);
                threadSv1.Abort();
                threadSv2.Abort();
            }
            catch
            {
            }

            #endregion ServerClosing

            #region ClientClosing

            try
            {
                client.Shutdown(SocketShutdown.Both);
                threadCl.Abort();
            }
            catch
            {
            }

            #endregion ClientClosing

            try
            {
                Environment.Exit(0);
            }
            catch { }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            btnCancleServer.Visible = false;
            btnCancleClient.Visible = false;
            lstMsg.Enabled = false;

            //----------

            IPHostEntry myHostInfo = Dns.Resolve(Dns.GetHostName());
            txtIPServer.Text = myHostInfo.AddressList[0].ToString();
            lblNameServer.Text = Dns.GetHostName();

            listnerSv = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            txtMsgServer.Enabled = false;
            btnSendMsgServer.Enabled = false;

            //----------

            txtServerIPClient.Text = "127.0.0.1";
            lblNameClient.Text = Dns.GetHostName();

            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            txtMsgClient.Enabled = false;
            btnSendMsgClient.Enabled = false;
        }
    }
}