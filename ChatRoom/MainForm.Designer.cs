﻿namespace ChatRoom
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.grbServer = new System.Windows.Forms.GroupBox();
            this.picBoxServer = new System.Windows.Forms.PictureBox();
            this.lblCommentServer = new System.Windows.Forms.Label();
            this.txtIPServer = new System.Windows.Forms.TextBox();
            this.lblServerIPServer = new System.Windows.Forms.Label();
            this.btnConnectServer = new System.Windows.Forms.Button();
            this.lblNameServer = new System.Windows.Forms.Label();
            this.lblServerNameServer = new System.Windows.Forms.Label();
            this.toolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnExit = new System.Windows.Forms.ToolStripButton();
            this.btnMinimized = new System.Windows.Forms.ToolStripButton();
            this.btnInfo = new System.Windows.Forms.ToolStripButton();
            this.btnSendMsgServer = new System.Windows.Forms.Button();
            this.txtMsgServer = new System.Windows.Forms.TextBox();
            this.toolStripLogo = new System.Windows.Forms.ToolStrip();
            this.lblProductLogo = new System.Windows.Forms.ToolStripLabel();
            this.lblMainProductName = new System.Windows.Forms.ToolStripLabel();
            this.superTabControlMain = new DevComponents.DotNetBar.SuperTabControl();
            this.stcPanelClient = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.panelClient = new System.Windows.Forms.Panel();
            this.btnSendMsgClient = new System.Windows.Forms.Button();
            this.txtMsgClient = new System.Windows.Forms.TextBox();
            this.grbClient = new System.Windows.Forms.GroupBox();
            this.lblNameClient = new System.Windows.Forms.Label();
            this.lblMyNameClient = new System.Windows.Forms.Label();
            this.picBoxClient = new System.Windows.Forms.PictureBox();
            this.txtServerIPClient = new System.Windows.Forms.TextBox();
            this.lblCommentClient = new System.Windows.Forms.Label();
            this.btnConnectClient = new System.Windows.Forms.Button();
            this.lblServerIPClient = new System.Windows.Forms.Label();
            this.superTabItemClient = new DevComponents.DotNetBar.SuperTabItem();
            this.stcPanelServer = new DevComponents.DotNetBar.SuperTabControlPanel();
            this.panelServer = new System.Windows.Forms.Panel();
            this.superTabItemServer = new DevComponents.DotNetBar.SuperTabItem();
            this.lstMsg = new System.Windows.Forms.ListBox();
            this.btnCancleServer = new System.Windows.Forms.Button();
            this.btnCancleClient = new System.Windows.Forms.Button();
            this.grbServer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxServer)).BeginInit();
            this.toolStripMenu.SuspendLayout();
            this.toolStripLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControlMain)).BeginInit();
            this.superTabControlMain.SuspendLayout();
            this.stcPanelClient.SuspendLayout();
            this.panelClient.SuspendLayout();
            this.grbClient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxClient)).BeginInit();
            this.stcPanelServer.SuspendLayout();
            this.panelServer.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbServer
            // 
            this.grbServer.BackColor = System.Drawing.Color.Transparent;
            this.grbServer.Controls.Add(this.btnCancleServer);
            this.grbServer.Controls.Add(this.picBoxServer);
            this.grbServer.Controls.Add(this.lblCommentServer);
            this.grbServer.Controls.Add(this.txtIPServer);
            this.grbServer.Controls.Add(this.lblServerIPServer);
            this.grbServer.Controls.Add(this.btnConnectServer);
            this.grbServer.Controls.Add(this.lblNameServer);
            this.grbServer.Controls.Add(this.lblServerNameServer);
            this.grbServer.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbServer.ForeColor = System.Drawing.Color.DarkGreen;
            this.grbServer.Location = new System.Drawing.Point(12, 15);
            this.grbServer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grbServer.Name = "grbServer";
            this.grbServer.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grbServer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grbServer.Size = new System.Drawing.Size(320, 140);
            this.grbServer.TabIndex = 0;
            this.grbServer.TabStop = false;
            this.grbServer.Text = "Server";
            // 
            // picBoxServer
            // 
            this.picBoxServer.BackgroundImage = global::ChatRoom.Properties.Resources.Pointers;
            this.picBoxServer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBoxServer.Location = new System.Drawing.Point(192, 85);
            this.picBoxServer.Name = "picBoxServer";
            this.picBoxServer.Size = new System.Drawing.Size(50, 50);
            this.picBoxServer.TabIndex = 10;
            this.picBoxServer.TabStop = false;
            // 
            // lblCommentServer
            // 
            this.lblCommentServer.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblCommentServer.ForeColor = System.Drawing.Color.DarkGreen;
            this.lblCommentServer.Location = new System.Drawing.Point(10, 100);
            this.lblCommentServer.Name = "lblCommentServer";
            this.lblCommentServer.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblCommentServer.Size = new System.Drawing.Size(176, 22);
            this.lblCommentServer.TabIndex = 9;
            this.lblCommentServer.Text = "می‌توانید گپی دوستانه را آغاز کنید!";
            this.lblCommentServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtIPServer
            // 
            this.txtIPServer.BackColor = System.Drawing.Color.SpringGreen;
            this.txtIPServer.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtIPServer.ForeColor = System.Drawing.Color.Blue;
            this.txtIPServer.Location = new System.Drawing.Point(160, 30);
            this.txtIPServer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtIPServer.Name = "txtIPServer";
            this.txtIPServer.Size = new System.Drawing.Size(150, 25);
            this.txtIPServer.TabIndex = 8;
            this.txtIPServer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblServerIPServer
            // 
            this.lblServerIPServer.AutoSize = true;
            this.lblServerIPServer.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblServerIPServer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblServerIPServer.Location = new System.Drawing.Point(6, 31);
            this.lblServerIPServer.Name = "lblServerIPServer";
            this.lblServerIPServer.Size = new System.Drawing.Size(134, 21);
            this.lblServerIPServer.TabIndex = 4;
            this.lblServerIPServer.Text = "Server IP (this Computer):";
            // 
            // btnConnectServer
            // 
            this.btnConnectServer.BackColor = System.Drawing.Color.Crimson;
            this.btnConnectServer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConnectServer.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnConnectServer.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnConnectServer.Location = new System.Drawing.Point(248, 97);
            this.btnConnectServer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnConnectServer.Name = "btnConnectServer";
            this.btnConnectServer.Size = new System.Drawing.Size(63, 28);
            this.btnConnectServer.TabIndex = 5;
            this.btnConnectServer.Text = "شروع";
            this.btnConnectServer.UseVisualStyleBackColor = false;
            this.btnConnectServer.Click += new System.EventHandler(this.btnConnectServer_Click);
            // 
            // lblNameServer
            // 
            this.lblNameServer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNameServer.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblNameServer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblNameServer.Location = new System.Drawing.Point(160, 59);
            this.lblNameServer.Name = "lblNameServer";
            this.lblNameServer.Size = new System.Drawing.Size(150, 21);
            this.lblNameServer.TabIndex = 7;
            this.lblNameServer.Text = "lblNameServer";
            this.lblNameServer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblServerNameServer
            // 
            this.lblServerNameServer.AutoSize = true;
            this.lblServerNameServer.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblServerNameServer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.lblServerNameServer.Location = new System.Drawing.Point(6, 59);
            this.lblServerNameServer.Name = "lblServerNameServer";
            this.lblServerNameServer.Size = new System.Drawing.Size(150, 21);
            this.lblServerNameServer.TabIndex = 6;
            this.lblServerNameServer.Text = "Server Name (this Computer):";
            // 
            // toolStripMenu
            // 
            this.toolStripMenu.BackColor = System.Drawing.Color.Transparent;
            this.toolStripMenu.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.toolStripMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnExit,
            this.btnMinimized,
            this.btnInfo});
            this.toolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStripMenu.Name = "toolStripMenu";
            this.toolStripMenu.Size = new System.Drawing.Size(344, 25);
            this.toolStripMenu.TabIndex = 2;
            this.toolStripMenu.Text = "toolStrip1";
            // 
            // btnExit
            // 
            this.btnExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnExit.Image = global::ChatRoom.Properties.Resources.btnExit;
            this.btnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(23, 22);
            this.btnExit.Text = "خروج";
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnMinimized
            // 
            this.btnMinimized.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnMinimized.Image = global::ChatRoom.Properties.Resources.btnMinimized;
            this.btnMinimized.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnMinimized.Name = "btnMinimized";
            this.btnMinimized.Size = new System.Drawing.Size(23, 22);
            this.btnMinimized.Text = "کمینه";
            this.btnMinimized.Click += new System.EventHandler(this.btnMinimized_Click);
            // 
            // btnInfo
            // 
            this.btnInfo.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnInfo.Image = global::ChatRoom.Properties.Resources.lblLogo;
            this.btnInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(23, 22);
            this.btnInfo.Text = "درباره ما";
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // btnSendMsgServer
            // 
            this.btnSendMsgServer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnSendMsgServer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSendMsgServer.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnSendMsgServer.Location = new System.Drawing.Point(260, 180);
            this.btnSendMsgServer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSendMsgServer.Name = "btnSendMsgServer";
            this.btnSendMsgServer.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnSendMsgServer.Size = new System.Drawing.Size(63, 30);
            this.btnSendMsgServer.TabIndex = 4;
            this.btnSendMsgServer.Text = "ارسال";
            this.btnSendMsgServer.UseVisualStyleBackColor = false;
            this.btnSendMsgServer.Click += new System.EventHandler(this.btnSendMsgServer_Click);
            // 
            // txtMsgServer
            // 
            this.txtMsgServer.Location = new System.Drawing.Point(22, 165);
            this.txtMsgServer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMsgServer.Multiline = true;
            this.txtMsgServer.Name = "txtMsgServer";
            this.txtMsgServer.Size = new System.Drawing.Size(230, 60);
            this.txtMsgServer.TabIndex = 3;
            this.txtMsgServer.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMsgServer_KeyPress);
            // 
            // toolStripLogo
            // 
            this.toolStripLogo.BackColor = System.Drawing.Color.Transparent;
            this.toolStripLogo.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.toolStripLogo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripLogo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblProductLogo,
            this.lblMainProductName});
            this.toolStripLogo.Location = new System.Drawing.Point(0, 25);
            this.toolStripLogo.Name = "toolStripLogo";
            this.toolStripLogo.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.toolStripLogo.Size = new System.Drawing.Size(344, 53);
            this.toolStripLogo.TabIndex = 5;
            this.toolStripLogo.Text = "toolStrip2";
            this.toolStripLogo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseDown);
            this.toolStripLogo.MouseHover += new System.EventHandler(this.toolStripLogo_MouseHover);
            this.toolStripLogo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDragObjects_MouseMove);
            // 
            // lblProductLogo
            // 
            this.lblProductLogo.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblProductLogo.AutoSize = false;
            this.lblProductLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("lblProductLogo.BackgroundImage")));
            this.lblProductLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.lblProductLogo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.lblProductLogo.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblProductLogo.ForeColor = System.Drawing.Color.Red;
            this.lblProductLogo.Name = "lblProductLogo";
            this.lblProductLogo.Size = new System.Drawing.Size(50, 50);
            this.lblProductLogo.Text = "لوگو";
            // 
            // lblMainProductName
            // 
            this.lblMainProductName.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.lblMainProductName.Font = new System.Drawing.Font("B Nazanin", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblMainProductName.ForeColor = System.Drawing.Color.Red;
            this.lblMainProductName.Name = "lblMainProductName";
            this.lblMainProductName.Size = new System.Drawing.Size(144, 50);
            this.lblMainProductName.Text = "گپ دوستانه";
            // 
            // superTabControlMain
            // 
            // 
            // 
            // 
            // 
            // 
            // 
            this.superTabControlMain.ControlBox.CloseBox.Name = "";
            // 
            // 
            // 
            this.superTabControlMain.ControlBox.MenuBox.Name = "";
            this.superTabControlMain.ControlBox.MenuBox.Visible = false;
            this.superTabControlMain.ControlBox.Name = "";
            this.superTabControlMain.ControlBox.SubItems.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabControlMain.ControlBox.MenuBox,
            this.superTabControlMain.ControlBox.CloseBox});
            this.superTabControlMain.Controls.Add(this.stcPanelClient);
            this.superTabControlMain.Controls.Add(this.stcPanelServer);
            this.superTabControlMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.superTabControlMain.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.superTabControlMain.ImageAlignment = DevComponents.DotNetBar.ImageAlignment.MiddleRight;
            this.superTabControlMain.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.superTabControlMain.Location = new System.Drawing.Point(0, 78);
            this.superTabControlMain.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.superTabControlMain.Name = "superTabControlMain";
            this.superTabControlMain.ReorderTabsEnabled = false;
            this.superTabControlMain.SelectedTabFont = new System.Drawing.Font("B Nazanin", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.superTabControlMain.SelectedTabIndex = 0;
            this.superTabControlMain.Size = new System.Drawing.Size(344, 280);
            this.superTabControlMain.TabFont = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.superTabControlMain.TabIndex = 6;
            this.superTabControlMain.TabLayoutType = DevComponents.DotNetBar.eSuperTabLayoutType.SingleLineFit;
            this.superTabControlMain.Tabs.AddRange(new DevComponents.DotNetBar.BaseItem[] {
            this.superTabItemServer,
            this.superTabItemClient});
            this.superTabControlMain.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.VisualStudio2008Dock;
            this.superTabControlMain.Text = "بخش‌های اصلی";
            this.superTabControlMain.TextAlignment = DevComponents.DotNetBar.eItemAlignment.Center;
            // 
            // stcPanelClient
            // 
            this.stcPanelClient.Controls.Add(this.panelClient);
            this.stcPanelClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stcPanelClient.Location = new System.Drawing.Point(0, 46);
            this.stcPanelClient.Name = "stcPanelClient";
            this.stcPanelClient.Size = new System.Drawing.Size(344, 234);
            this.stcPanelClient.TabIndex = 0;
            this.stcPanelClient.TabItem = this.superTabItemClient;
            // 
            // panelClient
            // 
            this.panelClient.BackColor = System.Drawing.Color.LightSeaGreen;
            this.panelClient.Controls.Add(this.btnSendMsgClient);
            this.panelClient.Controls.Add(this.txtMsgClient);
            this.panelClient.Controls.Add(this.grbClient);
            this.panelClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelClient.Location = new System.Drawing.Point(0, 0);
            this.panelClient.Name = "panelClient";
            this.panelClient.Size = new System.Drawing.Size(344, 234);
            this.panelClient.TabIndex = 2;
            // 
            // btnSendMsgClient
            // 
            this.btnSendMsgClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnSendMsgClient.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSendMsgClient.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnSendMsgClient.Location = new System.Drawing.Point(260, 180);
            this.btnSendMsgClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSendMsgClient.Name = "btnSendMsgClient";
            this.btnSendMsgClient.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnSendMsgClient.Size = new System.Drawing.Size(63, 30);
            this.btnSendMsgClient.TabIndex = 6;
            this.btnSendMsgClient.Text = "ارسال";
            this.btnSendMsgClient.UseVisualStyleBackColor = false;
            this.btnSendMsgClient.Click += new System.EventHandler(this.btnSendMsgClient_Click);
            // 
            // txtMsgClient
            // 
            this.txtMsgClient.Location = new System.Drawing.Point(22, 165);
            this.txtMsgClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtMsgClient.Multiline = true;
            this.txtMsgClient.Name = "txtMsgClient";
            this.txtMsgClient.Size = new System.Drawing.Size(230, 60);
            this.txtMsgClient.TabIndex = 5;
            this.txtMsgClient.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMsgClient_KeyPress);
            // 
            // grbClient
            // 
            this.grbClient.BackColor = System.Drawing.Color.Transparent;
            this.grbClient.Controls.Add(this.btnCancleClient);
            this.grbClient.Controls.Add(this.lblNameClient);
            this.grbClient.Controls.Add(this.lblMyNameClient);
            this.grbClient.Controls.Add(this.picBoxClient);
            this.grbClient.Controls.Add(this.txtServerIPClient);
            this.grbClient.Controls.Add(this.lblCommentClient);
            this.grbClient.Controls.Add(this.btnConnectClient);
            this.grbClient.Controls.Add(this.lblServerIPClient);
            this.grbClient.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbClient.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.grbClient.Location = new System.Drawing.Point(12, 15);
            this.grbClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grbClient.Name = "grbClient";
            this.grbClient.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.grbClient.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.grbClient.Size = new System.Drawing.Size(320, 140);
            this.grbClient.TabIndex = 1;
            this.grbClient.TabStop = false;
            this.grbClient.Text = "Client";
            // 
            // lblNameClient
            // 
            this.lblNameClient.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNameClient.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblNameClient.ForeColor = System.Drawing.Color.DarkRed;
            this.lblNameClient.Location = new System.Drawing.Point(160, 59);
            this.lblNameClient.Name = "lblNameClient";
            this.lblNameClient.Size = new System.Drawing.Size(150, 21);
            this.lblNameClient.TabIndex = 14;
            this.lblNameClient.Text = "lblNameThisPC";
            this.lblNameClient.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMyNameClient
            // 
            this.lblMyNameClient.AutoSize = true;
            this.lblMyNameClient.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblMyNameClient.ForeColor = System.Drawing.Color.DarkRed;
            this.lblMyNameClient.Location = new System.Drawing.Point(6, 59);
            this.lblMyNameClient.Name = "lblMyNameClient";
            this.lblMyNameClient.Size = new System.Drawing.Size(136, 21);
            this.lblMyNameClient.TabIndex = 13;
            this.lblMyNameClient.Text = "My Name (this Computer):";
            // 
            // picBoxClient
            // 
            this.picBoxClient.BackgroundImage = global::ChatRoom.Properties.Resources.Pointers;
            this.picBoxClient.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picBoxClient.Location = new System.Drawing.Point(192, 85);
            this.picBoxClient.Name = "picBoxClient";
            this.picBoxClient.Size = new System.Drawing.Size(50, 50);
            this.picBoxClient.TabIndex = 12;
            this.picBoxClient.TabStop = false;
            // 
            // txtServerIPClient
            // 
            this.txtServerIPClient.BackColor = System.Drawing.Color.LightSeaGreen;
            this.txtServerIPClient.Font = new System.Drawing.Font("B Nazanin", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtServerIPClient.ForeColor = System.Drawing.Color.DarkRed;
            this.txtServerIPClient.Location = new System.Drawing.Point(160, 30);
            this.txtServerIPClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtServerIPClient.Name = "txtServerIPClient";
            this.txtServerIPClient.Size = new System.Drawing.Size(150, 25);
            this.txtServerIPClient.TabIndex = 6;
            this.txtServerIPClient.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblCommentClient
            // 
            this.lblCommentClient.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblCommentClient.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblCommentClient.Location = new System.Drawing.Point(10, 100);
            this.lblCommentClient.Name = "lblCommentClient";
            this.lblCommentClient.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblCommentClient.Size = new System.Drawing.Size(176, 22);
            this.lblCommentClient.TabIndex = 11;
            this.lblCommentClient.Text = "می‌توانید به گپی دوستانه بپیوندید!";
            this.lblCommentClient.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnConnectClient
            // 
            this.btnConnectClient.BackColor = System.Drawing.Color.Lime;
            this.btnConnectClient.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConnectClient.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnConnectClient.Location = new System.Drawing.Point(248, 97);
            this.btnConnectClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnConnectClient.Name = "btnConnectClient";
            this.btnConnectClient.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnConnectClient.Size = new System.Drawing.Size(63, 28);
            this.btnConnectClient.TabIndex = 5;
            this.btnConnectClient.Text = "جستجو ...";
            this.btnConnectClient.UseVisualStyleBackColor = false;
            this.btnConnectClient.Click += new System.EventHandler(this.btnConnectClient_Click);
            // 
            // lblServerIPClient
            // 
            this.lblServerIPClient.AutoSize = true;
            this.lblServerIPClient.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblServerIPClient.ForeColor = System.Drawing.Color.DarkRed;
            this.lblServerIPClient.Location = new System.Drawing.Point(6, 31);
            this.lblServerIPClient.Name = "lblServerIPClient";
            this.lblServerIPClient.Size = new System.Drawing.Size(136, 21);
            this.lblServerIPClient.TabIndex = 3;
            this.lblServerIPClient.Text = "Server IP (For Connected):";
            // 
            // superTabItemClient
            // 
            this.superTabItemClient.AttachedControl = this.stcPanelClient;
            this.superTabItemClient.GlobalItem = false;
            this.superTabItemClient.Name = "superTabItemClient";
            this.superTabItemClient.Text = "Client";
            // 
            // stcPanelServer
            // 
            this.stcPanelServer.Controls.Add(this.panelServer);
            this.stcPanelServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stcPanelServer.Location = new System.Drawing.Point(0, 46);
            this.stcPanelServer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.stcPanelServer.Name = "stcPanelServer";
            this.stcPanelServer.Size = new System.Drawing.Size(344, 234);
            this.stcPanelServer.TabIndex = 10;
            this.stcPanelServer.TabItem = this.superTabItemServer;
            // 
            // panelServer
            // 
            this.panelServer.BackColor = System.Drawing.Color.SpringGreen;
            this.panelServer.Controls.Add(this.btnSendMsgServer);
            this.panelServer.Controls.Add(this.grbServer);
            this.panelServer.Controls.Add(this.txtMsgServer);
            this.panelServer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelServer.Location = new System.Drawing.Point(0, 0);
            this.panelServer.Name = "panelServer";
            this.panelServer.Size = new System.Drawing.Size(344, 234);
            this.panelServer.TabIndex = 0;
            // 
            // superTabItemServer
            // 
            this.superTabItemServer.AttachedControl = this.stcPanelServer;
            this.superTabItemServer.GlobalItem = false;
            this.superTabItemServer.ImageIndex = 0;
            this.superTabItemServer.Name = "superTabItemServer";
            this.superTabItemServer.Text = "Server";
            // 
            // lstMsg
            // 
            this.lstMsg.FormattingEnabled = true;
            this.lstMsg.Location = new System.Drawing.Point(12, 375);
            this.lstMsg.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lstMsg.Name = "lstMsg";
            this.lstMsg.Size = new System.Drawing.Size(320, 173);
            this.lstMsg.TabIndex = 7;
            // 
            // btnCancleServer
            // 
            this.btnCancleServer.BackColor = System.Drawing.Color.OrangeRed;
            this.btnCancleServer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancleServer.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnCancleServer.ForeColor = System.Drawing.Color.DarkGreen;
            this.btnCancleServer.Location = new System.Drawing.Point(248, -2);
            this.btnCancleServer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancleServer.Name = "btnCancleServer";
            this.btnCancleServer.Size = new System.Drawing.Size(63, 28);
            this.btnCancleServer.TabIndex = 11;
            this.btnCancleServer.Text = "انصراف";
            this.btnCancleServer.UseVisualStyleBackColor = false;
            this.btnCancleServer.Click += new System.EventHandler(this.btnCancleServer_Click);
            // 
            // btnCancleClient
            // 
            this.btnCancleClient.BackColor = System.Drawing.Color.SeaGreen;
            this.btnCancleClient.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancleClient.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnCancleClient.ForeColor = System.Drawing.Color.DarkMagenta;
            this.btnCancleClient.Location = new System.Drawing.Point(248, -2);
            this.btnCancleClient.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnCancleClient.Name = "btnCancleClient";
            this.btnCancleClient.Size = new System.Drawing.Size(63, 28);
            this.btnCancleClient.TabIndex = 15;
            this.btnCancleClient.Text = "انصراف";
            this.btnCancleClient.UseVisualStyleBackColor = false;
            this.btnCancleClient.Click += new System.EventHandler(this.btnCancleClient_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Aquamarine;
            this.ClientSize = new System.Drawing.Size(344, 561);
            this.ControlBox = false;
            this.Controls.Add(this.lstMsg);
            this.Controls.Add(this.superTabControlMain);
            this.Controls.Add(this.toolStripLogo);
            this.Controls.Add(this.toolStripMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ChatRoom 1.00";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmDragForm_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmDragForm_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmDragForm_MouseMove);
            this.grbServer.ResumeLayout(false);
            this.grbServer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxServer)).EndInit();
            this.toolStripMenu.ResumeLayout(false);
            this.toolStripMenu.PerformLayout();
            this.toolStripLogo.ResumeLayout(false);
            this.toolStripLogo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.superTabControlMain)).EndInit();
            this.superTabControlMain.ResumeLayout(false);
            this.stcPanelClient.ResumeLayout(false);
            this.panelClient.ResumeLayout(false);
            this.panelClient.PerformLayout();
            this.grbClient.ResumeLayout(false);
            this.grbClient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxClient)).EndInit();
            this.stcPanelServer.ResumeLayout(false);
            this.panelServer.ResumeLayout(false);
            this.panelServer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbServer;
        private System.Windows.Forms.ToolStrip toolStripMenu;
        private System.Windows.Forms.ToolStripButton btnExit;
        private System.Windows.Forms.ToolStripButton btnMinimized;
        private System.Windows.Forms.Label lblServerIPServer;
        private System.Windows.Forms.Label lblNameServer;
        private System.Windows.Forms.Label lblServerNameServer;
        private System.Windows.Forms.Button btnSendMsgServer;
        private System.Windows.Forms.TextBox txtMsgServer;
        private System.Windows.Forms.Button btnConnectServer;
        private System.Windows.Forms.ToolStrip toolStripLogo;
        private System.Windows.Forms.ToolStripLabel lblProductLogo;
        private System.Windows.Forms.ToolStripLabel lblMainProductName;
        private DevComponents.DotNetBar.SuperTabControl superTabControlMain;
        private DevComponents.DotNetBar.SuperTabControlPanel stcPanelServer;
        private DevComponents.DotNetBar.SuperTabItem superTabItemServer;
        private System.Windows.Forms.Panel panelServer;
        private System.Windows.Forms.ListBox lstMsg;
        private DevComponents.DotNetBar.SuperTabControlPanel stcPanelClient;
        private DevComponents.DotNetBar.SuperTabItem superTabItemClient;
        private System.Windows.Forms.Panel panelClient;
        private System.Windows.Forms.Button btnSendMsgClient;
        private System.Windows.Forms.TextBox txtMsgClient;
        private System.Windows.Forms.GroupBox grbClient;
        private System.Windows.Forms.TextBox txtServerIPClient;
        private System.Windows.Forms.Button btnConnectClient;
        private System.Windows.Forms.Label lblServerIPClient;
        private System.Windows.Forms.ToolStripButton btnInfo;
        private System.Windows.Forms.TextBox txtIPServer;
        private System.Windows.Forms.Label lblCommentServer;
        private System.Windows.Forms.PictureBox picBoxServer;
        private System.Windows.Forms.PictureBox picBoxClient;
        private System.Windows.Forms.Label lblCommentClient;
        private System.Windows.Forms.Label lblNameClient;
        private System.Windows.Forms.Label lblMyNameClient;
        private System.Windows.Forms.Button btnCancleServer;
        private System.Windows.Forms.Button btnCancleClient;
    }
}

