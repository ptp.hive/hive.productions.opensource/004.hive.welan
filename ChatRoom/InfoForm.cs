﻿using System;
using System.Windows.Forms;
//
using ChatRoom.Properties;

namespace ChatRoom
{
    public partial class InfoForm : Form
    {
        public InfoForm()
        {
            InitializeComponent();
        }

        private void btnRunProgram_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Enabled = true;
        }

        private void InfoForm_Load(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            Icon = Resources.InfoFrm;

            lblProductVersion.Text = Application.ProductVersion.Substring(0, 4);

            rchtxtAphorism.Text = "با سلام خدمت کاربران گرامی." +
                                  "\n== --- == --- == --- ==" +
                                  "\nبا گپ دوستانه می‌توان در محیطی صمیمی با رابط کاربری بهینه نیز به گفتگو با دوستان خود در محیطی کوچک پرداخت." +
                                  "\nپیغام‌هایی مناسب با هر بخش نیز نمایش داده می‌شود." +
                                    " برای تعامل بیشتر با برنامه، به پیغام‌ها توجه کنید." +
                                  "\n== --- == --- == --- ==" +
                                  "\nاجرا و نمایش صحیح برنامه‌ی گپ دوستانه، نیازمند موارد زیر می‌باشد:" +
                                  "\n۱. فونت B Nazanin" +
                                  "\n۲. حداقل رزولوشن ۷۶۸*۱۰۲۴" +
                                  "\n۳. Microsoft .NET Framework 3" +
                                  "\n۴. Microsoft Visual C++ 2013 Redistributable x86" +
                                  "\nلطفاً به وجود پیش‌نیازهای ذکر شده در ویندوز خود، بعد از فرآیند نصب توجه نمایید." +
                                  "\nدر صورت ناخوانا بودن نوشته‌ها، لطفاً مجموعه‌ی فونت‌های B Nazanin" +
                                    " همراه این برنامه را مجدداً نصب کنید." +
                                  "\n== --- == --- == --- ==" +
                                  "\nبرای نمایش بهتر برنامه، لطفا منطقه‌ی زمانی ویندوز خود را فارسی نمایید." +
                                  "\nRegion Format: Persian" +
                                  "\n== --- == --- == --- ==" +
                                  "\nاکثر ایرادهای منطقی و خطاهای رایج این برنامه گرفته شده است." +
                                  "\nدر صورت بروز مشکلی فنی، امیدوارم اینجانب را به بزرگواری خود ببخشید." +
                                    " می‌توانید از طریق ایمیل و یا پیام‌رسان، مشکل خود را مطرح کنید." +
                                  "\n== --- == --- == --- ==" +
                                  "\nبا تشکر" +
                                  "\nMehdi MzIT" +
                                  "\nCopyright © 2015";

            rchtxtInTheFuture.Text = "در نسخه‌های آینده منتظر موارد زیر باشید:" +
                                     "\n۱. پوسته‌های متنوع‌تر" +
                                     "\n۲. محیطی با رابط کاربری بهینه‌تر" +
                                     "\n۳. انعطاف بیشتر برنامه در تعامل با کاربر";

            Cursor = Cursors.Default;
        }
    }
}
