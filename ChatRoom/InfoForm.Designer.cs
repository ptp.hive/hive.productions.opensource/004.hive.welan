﻿namespace ChatRoom
{
    partial class InfoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpstiAbout = new System.Windows.Forms.TableLayoutPanel();
            this.tlpstiAboutDetails = new System.Windows.Forms.TableLayoutPanel();
            this.tlpAboutUs = new System.Windows.Forms.TableLayoutPanel();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.tlpANP = new System.Windows.Forms.TableLayoutPanel();
            this.tlpVersion = new System.Windows.Forms.TableLayoutPanel();
            this.lblVersion = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblProductVersion = new System.Windows.Forms.Label();
            this.linkLblEmail = new System.Windows.Forms.LinkLabel();
            this.lblProductName = new System.Windows.Forms.Label();
            this.tlpAboutDescription = new System.Windows.Forms.TableLayoutPanel();
            this.grbInTheFuture = new System.Windows.Forms.GroupBox();
            this.rchtxtInTheFuture = new System.Windows.Forms.RichTextBox();
            this.grbAphorism = new System.Windows.Forms.GroupBox();
            this.rchtxtAphorism = new System.Windows.Forms.RichTextBox();
            this.btnRunProgram = new System.Windows.Forms.Button();
            this.tlpstiAbout.SuspendLayout();
            this.tlpstiAboutDetails.SuspendLayout();
            this.tlpAboutUs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.tlpANP.SuspendLayout();
            this.tlpVersion.SuspendLayout();
            this.tlpAboutDescription.SuspendLayout();
            this.grbInTheFuture.SuspendLayout();
            this.grbAphorism.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpstiAbout
            // 
            this.tlpstiAbout.BackColor = System.Drawing.Color.Transparent;
            this.tlpstiAbout.ColumnCount = 3;
            this.tlpstiAbout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tlpstiAbout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76F));
            this.tlpstiAbout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tlpstiAbout.Controls.Add(this.tlpstiAboutDetails, 1, 1);
            this.tlpstiAbout.Controls.Add(this.btnRunProgram, 0, 0);
            this.tlpstiAbout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpstiAbout.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tlpstiAbout.Location = new System.Drawing.Point(0, 0);
            this.tlpstiAbout.Name = "tlpstiAbout";
            this.tlpstiAbout.RowCount = 3;
            this.tlpstiAbout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tlpstiAbout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 74F));
            this.tlpstiAbout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tlpstiAbout.Size = new System.Drawing.Size(784, 461);
            this.tlpstiAbout.TabIndex = 1;
            // 
            // tlpstiAboutDetails
            // 
            this.tlpstiAboutDetails.BackColor = System.Drawing.Color.SteelBlue;
            this.tlpstiAboutDetails.ColumnCount = 1;
            this.tlpstiAboutDetails.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpstiAboutDetails.Controls.Add(this.tlpAboutUs, 0, 0);
            this.tlpstiAboutDetails.Controls.Add(this.tlpAboutDescription, 0, 2);
            this.tlpstiAboutDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpstiAboutDetails.Location = new System.Drawing.Point(98, 62);
            this.tlpstiAboutDetails.Name = "tlpstiAboutDetails";
            this.tlpstiAboutDetails.RowCount = 3;
            this.tlpstiAboutDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tlpstiAboutDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlpstiAboutDetails.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpstiAboutDetails.Size = new System.Drawing.Size(589, 335);
            this.tlpstiAboutDetails.TabIndex = 2;
            // 
            // tlpAboutUs
            // 
            this.tlpAboutUs.BackColor = System.Drawing.Color.SpringGreen;
            this.tlpAboutUs.ColumnCount = 3;
            this.tlpAboutUs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tlpAboutUs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tlpAboutUs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAboutUs.Controls.Add(this.picLogo, 0, 0);
            this.tlpAboutUs.Controls.Add(this.tlpANP, 2, 0);
            this.tlpAboutUs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAboutUs.Location = new System.Drawing.Point(3, 3);
            this.tlpAboutUs.Name = "tlpAboutUs";
            this.tlpAboutUs.RowCount = 1;
            this.tlpAboutUs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAboutUs.Size = new System.Drawing.Size(583, 128);
            this.tlpAboutUs.TabIndex = 3;
            // 
            // picLogo
            // 
            this.picLogo.BackColor = System.Drawing.Color.Yellow;
            this.picLogo.BackgroundImage = global::ChatRoom.Properties.Resources.lblLogo;
            this.picLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picLogo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLogo.Location = new System.Drawing.Point(456, 3);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(124, 122);
            this.picLogo.TabIndex = 0;
            this.picLogo.TabStop = false;
            // 
            // tlpANP
            // 
            this.tlpANP.ColumnCount = 1;
            this.tlpANP.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpANP.Controls.Add(this.tlpVersion, 0, 1);
            this.tlpANP.Controls.Add(this.lblProductName, 0, 0);
            this.tlpANP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpANP.Location = new System.Drawing.Point(3, 3);
            this.tlpANP.Name = "tlpANP";
            this.tlpANP.RowCount = 2;
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tlpANP.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tlpANP.Size = new System.Drawing.Size(372, 122);
            this.tlpANP.TabIndex = 31;
            // 
            // tlpVersion
            // 
            this.tlpVersion.ColumnCount = 3;
            this.tlpVersion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tlpVersion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 240F));
            this.tlpVersion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpVersion.Controls.Add(this.lblVersion, 0, 0);
            this.tlpVersion.Controls.Add(this.lblEmail, 0, 1);
            this.tlpVersion.Controls.Add(this.lblProductVersion, 1, 0);
            this.tlpVersion.Controls.Add(this.linkLblEmail, 1, 1);
            this.tlpVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpVersion.Font = new System.Drawing.Font("B Nazanin", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.tlpVersion.Location = new System.Drawing.Point(3, 51);
            this.tlpVersion.Name = "tlpVersion";
            this.tlpVersion.RowCount = 2;
            this.tlpVersion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpVersion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpVersion.Size = new System.Drawing.Size(366, 68);
            this.tlpVersion.TabIndex = 32;
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(269, 5);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(94, 24);
            this.lblVersion.TabIndex = 321;
            this.lblVersion.Text = "نسخه برنامه:";
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEmail
            // 
            this.lblEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(269, 39);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(94, 24);
            this.lblEmail.TabIndex = 333;
            this.lblEmail.Text = "ایمیل سازنده:";
            this.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProductVersion
            // 
            this.lblProductVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProductVersion.AutoSize = true;
            this.lblProductVersion.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblProductVersion.Location = new System.Drawing.Point(29, 4);
            this.lblProductVersion.Name = "lblProductVersion";
            this.lblProductVersion.Size = new System.Drawing.Size(234, 25);
            this.lblProductVersion.TabIndex = 322;
            this.lblProductVersion.Text = "۱.۰۰";
            this.lblProductVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // linkLblEmail
            // 
            this.linkLblEmail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLblEmail.AutoSize = true;
            this.linkLblEmail.Location = new System.Drawing.Point(29, 39);
            this.linkLblEmail.Name = "linkLblEmail";
            this.linkLblEmail.Size = new System.Drawing.Size(234, 24);
            this.linkLblEmail.TabIndex = 334;
            this.linkLblEmail.TabStop = true;
            this.linkLblEmail.Text = "MehdiMzITProductions@GMail.com";
            this.linkLblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProductName
            // 
            this.lblProductName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblProductName.AutoSize = true;
            this.lblProductName.Font = new System.Drawing.Font("B Nazanin", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblProductName.ForeColor = System.Drawing.Color.Red;
            this.lblProductName.Location = new System.Drawing.Point(3, 4);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(366, 39);
            this.lblProductName.TabIndex = 311;
            this.lblProductName.Text = "گپ دوستانه";
            // 
            // tlpAboutDescription
            // 
            this.tlpAboutDescription.ColumnCount = 3;
            this.tlpAboutDescription.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAboutDescription.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpAboutDescription.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpAboutDescription.Controls.Add(this.grbInTheFuture, 2, 0);
            this.tlpAboutDescription.Controls.Add(this.grbAphorism, 0, 0);
            this.tlpAboutDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpAboutDescription.Location = new System.Drawing.Point(3, 170);
            this.tlpAboutDescription.Name = "tlpAboutDescription";
            this.tlpAboutDescription.RowCount = 1;
            this.tlpAboutDescription.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpAboutDescription.Size = new System.Drawing.Size(583, 162);
            this.tlpAboutDescription.TabIndex = 2;
            // 
            // grbInTheFuture
            // 
            this.grbInTheFuture.Controls.Add(this.rchtxtInTheFuture);
            this.grbInTheFuture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbInTheFuture.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbInTheFuture.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.grbInTheFuture.Location = new System.Drawing.Point(3, 3);
            this.grbInTheFuture.Name = "grbInTheFuture";
            this.grbInTheFuture.Size = new System.Drawing.Size(276, 156);
            this.grbInTheFuture.TabIndex = 5;
            this.grbInTheFuture.TabStop = false;
            this.grbInTheFuture.Text = "در آینده";
            // 
            // rchtxtInTheFuture
            // 
            this.rchtxtInTheFuture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rchtxtInTheFuture.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rchtxtInTheFuture.ForeColor = System.Drawing.Color.Blue;
            this.rchtxtInTheFuture.Location = new System.Drawing.Point(3, 30);
            this.rchtxtInTheFuture.Name = "rchtxtInTheFuture";
            this.rchtxtInTheFuture.ReadOnly = true;
            this.rchtxtInTheFuture.ShowSelectionMargin = true;
            this.rchtxtInTheFuture.Size = new System.Drawing.Size(270, 123);
            this.rchtxtInTheFuture.TabIndex = 0;
            this.rchtxtInTheFuture.Text = "";
            // 
            // grbAphorism
            // 
            this.grbAphorism.Controls.Add(this.rchtxtAphorism);
            this.grbAphorism.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbAphorism.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbAphorism.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.grbAphorism.Location = new System.Drawing.Point(305, 3);
            this.grbAphorism.Name = "grbAphorism";
            this.grbAphorism.Size = new System.Drawing.Size(275, 156);
            this.grbAphorism.TabIndex = 4;
            this.grbAphorism.TabStop = false;
            this.grbAphorism.Text = "سخنی کوتاه";
            // 
            // rchtxtAphorism
            // 
            this.rchtxtAphorism.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rchtxtAphorism.Font = new System.Drawing.Font("B Nazanin", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.rchtxtAphorism.ForeColor = System.Drawing.Color.Blue;
            this.rchtxtAphorism.Location = new System.Drawing.Point(3, 30);
            this.rchtxtAphorism.Name = "rchtxtAphorism";
            this.rchtxtAphorism.ReadOnly = true;
            this.rchtxtAphorism.ShowSelectionMargin = true;
            this.rchtxtAphorism.Size = new System.Drawing.Size(269, 123);
            this.rchtxtAphorism.TabIndex = 0;
            this.rchtxtAphorism.Text = "";
            // 
            // btnRunProgram
            // 
            this.btnRunProgram.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRunProgram.AutoSize = true;
            this.btnRunProgram.BackColor = System.Drawing.Color.LimeGreen;
            this.btnRunProgram.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRunProgram.Font = new System.Drawing.Font("B Nazanin", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.btnRunProgram.ForeColor = System.Drawing.Color.Purple;
            this.btnRunProgram.Location = new System.Drawing.Point(693, 12);
            this.btnRunProgram.Name = "btnRunProgram";
            this.btnRunProgram.Size = new System.Drawing.Size(88, 35);
            this.btnRunProgram.TabIndex = 1;
            this.btnRunProgram.Text = "ادامه برنامه";
            this.btnRunProgram.UseVisualStyleBackColor = false;
            this.btnRunProgram.Click += new System.EventHandler(this.btnRunProgram_Click);
            // 
            // InfoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.Goldenrod;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.ControlBox = false;
            this.Controls.Add(this.tlpstiAbout);
            this.Name = "InfoForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.InfoForm_Load);
            this.tlpstiAbout.ResumeLayout(false);
            this.tlpstiAbout.PerformLayout();
            this.tlpstiAboutDetails.ResumeLayout(false);
            this.tlpAboutUs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.tlpANP.ResumeLayout(false);
            this.tlpANP.PerformLayout();
            this.tlpVersion.ResumeLayout(false);
            this.tlpVersion.PerformLayout();
            this.tlpAboutDescription.ResumeLayout(false);
            this.grbInTheFuture.ResumeLayout(false);
            this.grbAphorism.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpstiAbout;
        private System.Windows.Forms.TableLayoutPanel tlpstiAboutDetails;
        private System.Windows.Forms.TableLayoutPanel tlpAboutUs;
        private System.Windows.Forms.PictureBox picLogo;
        private System.Windows.Forms.TableLayoutPanel tlpANP;
        private System.Windows.Forms.TableLayoutPanel tlpVersion;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblProductVersion;
        private System.Windows.Forms.LinkLabel linkLblEmail;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.TableLayoutPanel tlpAboutDescription;
        private System.Windows.Forms.GroupBox grbInTheFuture;
        private System.Windows.Forms.RichTextBox rchtxtInTheFuture;
        private System.Windows.Forms.GroupBox grbAphorism;
        private System.Windows.Forms.RichTextBox rchtxtAphorism;
        private System.Windows.Forms.Button btnRunProgram;
    }
}